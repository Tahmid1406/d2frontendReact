import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Sidebar from './Components/Sidebar';
import Form from './Components/Form';


function App() {
  return (
    <div className='container-fluid'>
        <div className="row">
            <div className="col-md-2 sidebar-container">
                <Sidebar />
            </div>

            <div className="col-md-10 form-container">
                <Form ></Form>
            </div>
        </div>

 
             



    </div>
  );
}

export default App;
