
export const FakeColumn = [
    {
        Header : 'id',
        accessor: 'id'
    },
    {
        Header: 'username',
        accessor: 'username'
    },
    {
        Header: 'email',
        accessor: 'email'
    }
]