import React from 'react'
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import HomeIcon from '@material-ui/icons/Home';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';

export const SidebarData = [

    {
        title: 'Home',
        link : '/home',
        icon : <HomeIcon />,   
    },
    {
        title: 'Learn React',
        link : 'https://scrimba.com/playlist/p7P5Hd',
        icon : <OpenInNewIcon />,   
    },

    {
        title: 'Learn SASS',
        link : 'https://www.w3schools.com/sass/default.php',
        icon : <OpenInNewIcon />,
    },

    {
        title: 'Learn HTML',
        link : 'https://www.w3schools.com/html/default.asp',
        icon : <OpenInNewIcon />,
    },
    {
        title: 'Learn CSS',
        link : 'https://www.w3schools.com/css/default.asp',
        icon : <OpenInNewIcon />,
    },
     {
        title: 'Users',
        link : '#',
        icon : <SupervisedUserCircleIcon />,
    }
]
