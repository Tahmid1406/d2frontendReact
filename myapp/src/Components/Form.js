import React, {useState} from 'react'
import '../App.css'
import { DropdownData } from '../Data/DropdownData';


let index = 1
const Form = () => {
    const [erros, setErros] = useState({
    username: '',
    email: '',
    password: '',
    conPass: ''
    }); 

    
    function validateEmail(email) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    const handleEmailChange = (e) => {
        if (!validateEmail(e.target.value)) {
            setErros({
                ...erros,
            email: "This is not a valid email address!!"
        });
        }else {
            setErros({
                ...erros,
                email: ''
            });

        }
    }

    const handleNameChange = (e) => {
        if (e.target.value.trim().length < 4) {
            setErros({
                ...erros,
            username: "Name must be more than four characters long!!"
        });
        }else if (e.target.value.trim().length > 25) {
            setErros({
                ...erros,
            username: "Name is too long!!"
        });
        }else {
            setErros({
                ...erros,
                username: ''
            });
        }
    };

    const handlePassChange = (e) => {
        if (e.target.value.trim().length < 8) {
            setErros({
                ...erros,
                password: "Password can't be less than 8 characters!!"
        });
        }else if (e.target.value.trim().length > 20) {
            setErros({
                ...erros,
                password: "Password can't be more than 20 characters!!"
        });
        }else {
            setErros({
                ...erros,
                password: ''
            });
        }
    };

    const handleConPassChange = (e) => {
        let inputPass = document.form.passInput.value;
        if (!(e.target.value.trim() === inputPass)) {
            setErros({
                ...erros,
                conPass: "Confirmation of password must match!!"
        });
        }else {
            setErros({
                ...erros,
                conPass: ''
            });
        }
    };


  
    

    
    const handleSubmit = (e) => {

        let inputName = document.form.nameInput.value;
        let inputMail = document.form.mailInput.value;
        let inputcolor = document.form.colorInput.value;
        let inputPass = document.form.passInput.value;
        let inputConPass = document.form.conPassInput.value;
        
        if( inputName === ''){
            alert('You must enter a name')
        }else if(inputMail === ''){
            alert('You must provide an email')
        }else if(inputcolor === 'Your favourite color'){
            alert('you must pick a color')
        }else if(inputPass === '') {
            alert("You must enter a password")
        }else if(inputConPass === ''){
            alert('check your confirmation password')
        }else{

            let tr = document.createElement('tr')

            let td1 = tr.appendChild(document.createElement('td'))
            let td2 = tr.appendChild(document.createElement('td'))
            let td3 = tr.appendChild(document.createElement('td'))
            let td4 = tr.appendChild(document.createElement('td'))
            
            let colorName = DropdownData[inputcolor-1].optionName
            td1.innerHTML = index
            td2.innerHTML = inputName
            td3.innerHTML = inputMail
            td4.innerHTML = colorName

            console.log(inputcolor)
            tr.style.backgroundColor = DropdownData[inputcolor-1].optionName
            
            document.getElementById('user-table').appendChild(tr)
            index++
            document.form.reset();
        }

        
    }


    return (
        
        <div className="container-fluid">
            <div className="row">
                <div className="col-md-6">
                    <form className='form-body' method='POST' name='form'>
                        <h2 className='text-center'>User Registation</h2>
                        
                        <div className="mb-3">
                            <label htmlFor="exampleInputName" className="form-label">Full Name</label>

                            <input 
                                type="text" 
                                className="form-control" 
                                id="exampleInputName" 
                                placeholder='enter your full name'
                                onChange={handleNameChange}
                                name='nameInput'
                            ></input>
                            <span className='error-message'>{erros.username}</span>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="exampleInputEmail1" className="form-label">Email Address</label>
                            <input type="email" 
                                    className="form-control" 
                                    id="exampleInputEmail1" 
                                    placeholder='enter a valid email address' 
                                    onChange={handleEmailChange}
                                    name='mailInput'
                            ></input>
                            <span className='error-message'>{erros.email}</span>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="exampleInputColor" className="form-label">Select Your Favourite Color</label>

                            <select defaultValue={'DEFAULT'} className="form-select"  name='colorInput'>
                                <option defaultValue="DEFAULT">Your favourite color</option>
                                {DropdownData.map((value,key) =>{
                                    return (
                                        <option key={key} value={value.key} >{value.optionName}</option>
                                    );
                                })}
                            </select>

                        </div>

                        <div className="mb-3">
                            <label htmlFor="exampleInputPass" className="form-label">Password</label>
                            <input 
                                type="password" 
                                className="form-control" 
                                id="exampleInputPass" 
                                placeholder='enter password'
                                onChange={handlePassChange}
                                name='passInput'
                            ></input>
                            <span className='error-message'>{erros.password}</span>
                        </div>

                        <div className="mb-3">
                            <label htmlFor="exampleInputPassCon" className="form-label">Confirm password</label>
                            <input 
                                type="password" 
                                className="form-control" 
                                id="exampleInputPassCon" 
                                placeholder='enter your password again'
                                name='conPassInput'
                                onChange={handleConPassChange}
                            ></input>
                            <span className='error-message'>{erros.conPass}</span>
                        </div>
                        <div className="text-center">
                            <button type="button" className="btn btn-primary text-center form-submit-button" onClick={handleSubmit}>SUBMIT</button>
                        </div>
                        

                    </form>
                </div>

               



                <div className="col-md-6">
                    <table className="table" name='tbl' id='user-table'>
                            <thead>
                                <tr>
                                <th align='left' scope="col">SL. No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Mail</th>
                                <th scope="col">Color Picked</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>
                </div>

            </div>
        </div>
    )
}

export default Form
