
import React from 'react'
import Button from '@material-ui/core/Button'



const ButtonComponent = (props) => {

    function ButtonEvent(){
        alert("Button Works");
    }
    
    return(
        <div>
            <Button variant={props.variant} color={props.color} size={props.size} onClick={ButtonEvent} >{props.insideText}</Button>
        </div>
    ); 
    
}



export default ButtonComponent