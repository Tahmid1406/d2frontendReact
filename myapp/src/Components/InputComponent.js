import React from 'react'

const InputComponent = (props) => {


    return (
        <input 
            type={props.type} 
            className={props.className} 
            id={props.id} 
            placeholder={props.placeholder}
        ></input>
    )
}

export default InputComponent
