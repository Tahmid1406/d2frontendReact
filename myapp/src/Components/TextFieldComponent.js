import React from 'react'
import TextField from '@material-ui/core/TextField'


const TextFieldComponent = (props) => {

    return (
        <TextField label={props.label} variant={props.variant} type={props.type}/>
    ); 
}

export default TextFieldComponent