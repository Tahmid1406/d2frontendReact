import React from 'react'
import { DropdownData } from '../Data/DropdownData';
import '../App.css';


const DrowdownComponent = (props) => {
    return (
        <select defaultValue={'DEFAULT'} className="form-select" >
            <option defaultValue="DEFAULT">{props.dName}</option>
            {DropdownData.map((value,key) =>{
                return (
                    <option key={key} value={value.key} >{value.optionName}</option>
                );
            })}
        </select>
    );
}

export default DrowdownComponent


