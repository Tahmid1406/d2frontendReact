import React from 'react'
import '../App.css'
import { SidebarData } from '../Data/SidebarData.js'

const Sidebar = () => {
    return (
        <div className='sidebar'>
            <ul className='sidebarList'>
                {SidebarData.map((val,key) => {
                    return (
                        <li key={key}  
                            className='sidebarRow' 
                            id={window.location.pathname == val.link ? "active" : "" }
                            onClick={() => window.open(val.link, '_blank').focus()}
                        >
                            <div id='icon'>{val.icon}</div>
                            <div id='title'>{val.title}</div>                 
                        </li>
                    );
                })}
            </ul>
        </div>
    )
}

export default Sidebar
